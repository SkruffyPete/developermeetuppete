﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper.Abstractions;
using DeveloperMeetupPete.Constants;
using DeveloperMeetupPete.Models.Meetings;
using DeveloperMeetupPete.Providers;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace DeveloperMeetupPete.Tests.Providers
{
    public class MeetingProviderTests
    {
        private readonly MeetingProvider _meetingProvider;
        private readonly IDbExecutor _executor;
        private readonly ILogger _logger;

        public MeetingProviderTests()
        {
            var dbExecutorFactory = Substitute.For<IDbExecutorFactory>();
            _executor = Substitute.For<IDbExecutor>();
            dbExecutorFactory.CreateExecutor().Returns(_executor);

            _logger = Substitute.For<ILogger>();

            _meetingProvider = new MeetingProvider(dbExecutorFactory, _logger);
        }

        private static MeetingInfo GetMeetingInfo(int meetingId)
        {
            return new MeetingInfo()
            {
                MeetingId = 1,
                MeetingSubject = $"Meeting {meetingId}",
                MeetingDetail = $"Meeting {meetingId} detail",
                Price = 0,
                VenueName = $"Meeting {meetingId} venue",
                StartTime = DateTime.Today,
                EndTime = DateTime.Today
            };
        }

        [Fact]
        public async Task retrieve_meeting_returns_correct_object()
        {
            // Arrange
            var expectedMeeting = GetMeetingInfo(1);

            _executor.QueryFirstOrDefaultAsync<MeetingInfo>(CommonConstants.ProcGetMeetings, Arg.Any<object>(),
                Arg.Any<IDbTransaction>(), Arg.Any<int?>(), CommandType.StoredProcedure).Returns(expectedMeeting);

            // Act
            var meetingInfo = await _meetingProvider.RetrieveMeeting(1).ConfigureAwait(false);

            // Assert
            meetingInfo.Should().BeEquivalentTo(expectedMeeting);
        }

        [Fact]
        public async Task retrieve_meeting_calls_database_once()
        {
            // Arrange
            

            // Act
            var meetingInfo = await _meetingProvider.RetrieveMeeting(1).ConfigureAwait(false);

            // Assert
            await _executor.Received(1).QueryFirstOrDefaultAsync<MeetingInfo>(CommonConstants.ProcGetMeetings, Arg.Any<object>(),
                Arg.Any<IDbTransaction>(), Arg.Any<int?>(), CommandType.StoredProcedure).ConfigureAwait(false);
        }

        [Fact]
        public async Task retrieve_meeting_returns_null_when_no_value()
        {
            // Arrange


            // Act
            var meetingInfo = await _meetingProvider.RetrieveMeeting(1).ConfigureAwait(false);

            // Assert
            meetingInfo.Should().Be(null);
        }


    }
}
