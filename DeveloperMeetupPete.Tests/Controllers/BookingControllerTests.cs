﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using DeveloperMeetupPete.Constants;
using DeveloperMeetupPete.Controllers;
using DeveloperMeetupPete.Helpers.Interfaces;
using DeveloperMeetupPete.Models.Bookings;
using DeveloperMeetupPete.Models.DbModels;
using DeveloperMeetupPete.Models.Meetings;
using DeveloperMeetupPete.Providers.Interfaces;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace DeveloperMeetupPete.Tests.Controllers
{
    public class BookingControllerTests
    {
        private readonly BookingController _bookingController;
        private readonly IBookingProvider _bookingProvider;
        private readonly IMeetingProvider _meetingProvider;
        private readonly IPaymentProvider _paymentProvider;
        private readonly ICalculationHelper _calculationHelper;
        private readonly ILogger _logger;

        public BookingControllerTests()
        {
            _bookingProvider = Substitute.For<IBookingProvider>();
            _meetingProvider = Substitute.For<IMeetingProvider>();
            _paymentProvider = Substitute.For<IPaymentProvider>();
            _calculationHelper = Substitute.For<ICalculationHelper>();
            _logger = Substitute.For<ILogger>();

            _bookingController = new BookingController(_bookingProvider, _meetingProvider, _paymentProvider, _calculationHelper, _logger);
        }

        private ProvisionalBooking GetProvisionalBooking(int number)
        {
            var booking = new ProvisionalBooking
            {
                SeatBookings = new List<ProvisionalSeatBooking>()
            };

            var count = 1;

            while (count <= number)
            {
                booking.SeatBookings.Add(new ProvisionalSeatBooking
                {
                    MeetingId = count,
                    SeatNumber = $"{count * 100}",
                    SeatRow = NumberToString(count)
                });

                count++;
            }

            return booking;
        }
        
        private string NumberToString(int number)
        {
            var c = (char)(97 + number - 1);

            return c.ToString();
        }

        [Fact]
        public async Task create_booking_with_no_seats_returns_badresult()
        {
            // Arrange
            var booking = GetProvisionalBooking(0);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [Fact]
        public async Task create_booking_with_no_seats_returns_correct_badresult_message()
        {
            // Arrange
            var booking = GetProvisionalBooking(0);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>().Which.Message.Should().Be(CommonConstants.NoSeatsResponse);
        }

        [Fact]
        public async Task create_booking_with_too_many_seats_returns_badresult()
        {
            // Arrange
            var booking = GetProvisionalBooking(CommonConstants.MaximumSeatsPerBooking + 1);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [Fact]
        public async Task create_booking_with_too_many_seats_returns_correct_badresult_message()
        {
            // Arrange
            var booking = GetProvisionalBooking(CommonConstants.MaximumSeatsPerBooking + 1);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>().Which.Message.Should().Be(CommonConstants.MaximumSeatsResponse);
        }

        [Fact]
        public async Task create_booking_with_duplicate_seats_returns_badresult()
        {
            // Arrange
            var booking = GetProvisionalBooking(1);
            booking.SeatBookings.AddRange(GetProvisionalBooking(1).SeatBookings);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [Fact]
        public async Task create_booking_with_duplicate_seats_returns_correct_badresult_message()
        {
            // Arrange
            var booking = GetProvisionalBooking(1);
            booking.SeatBookings.AddRange(GetProvisionalBooking(1).SeatBookings);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>().Which.Message.Should().Be(CommonConstants.DuplicateSeatsResponse);
        }

        [Fact]
        public async Task create_booking_with_invalid_meeting_returns_badresult()
        {
            // Arrange
            var booking = GetProvisionalBooking(CommonConstants.MaximumSeatsPerBooking - 1);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [Fact]
        public async Task create_booking_with_invalid_meeting_returns_correct_badresult_message()
        {
            // Arrange
            var booking = GetProvisionalBooking(CommonConstants.MaximumSeatsPerBooking - 1);

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>().Which.Message.Should().Be(CommonConstants.MeetingNotFoundResponse);
        }

        [Fact]
        public async Task create_booking_with_invalid_seats_returns_badresult()
        {
            // Arrange
            var booking = GetProvisionalBooking(1);
            
            _meetingProvider.RetrieveMeeting(1).Returns(new MeetingInfo
            {
                MeetingId = 1
            });

            _bookingProvider.ValidateSeats(Arg.Any<List<ProvisionalSeatBooking>>()).Returns(new List<SeatValidationResponse>
            {
                new SeatValidationResponse
                {
                    Available = false
                }
            });

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [Fact]
        public async Task create_booking_with_invalid_seats_returns_correct_badresult_message()
        {
            // Arrange
            var booking = GetProvisionalBooking(1);

            _meetingProvider.RetrieveMeeting(Arg.Any<int>()).Returns(new MeetingInfo
            {
                MeetingId = 1
            });

            _bookingProvider.ValidateSeats(Arg.Any<List<ProvisionalSeatBooking>>()).Returns(new List<SeatValidationResponse>
            {
                new SeatValidationResponse
                {
                    Available = false
                }
            });

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<BadRequestErrorMessageResult>().Which.Message.Should().Be(CommonConstants.SeatsUnavailableResponse);
        }

        [Fact]
        public async Task create_booking_with_correct_data_returns_ok()
        {
            // Arrange
            var booking = GetProvisionalBooking(1);

            _meetingProvider.RetrieveMeeting(1).Returns(new MeetingInfo
            {
                MeetingId = 1
            });

            _bookingProvider.ValidateSeats(Arg.Any<List<ProvisionalSeatBooking>>()).Returns(new List<SeatValidationResponse>
            {
                new SeatValidationResponse
                {
                    Available = true
                }
            });

            _bookingProvider.SaveProvisionalBooking(Arg.Any<ProvisionalBooking>()).Returns(10);
            _bookingProvider.RetrieveBooking<BookingRetrieved>(Arg.Any<int>()).Returns(new BookingRetrieved());
            _bookingProvider.RetrieveSeatBookings<SeatBooking>(Arg.Any<int>()).Returns(new List<SeatBooking>());

            // Act
            var response = await _bookingController.CreateBooking(booking).ConfigureAwait(false);

            // Assert
            response.Should().BeOfType<OkNegotiatedContentResult<BookingRetrieved>>();
        }
    }
}
