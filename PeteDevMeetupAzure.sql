CREATE USER Api WITH PASSWORD = 'ZupaTest1234!';
GO

CREATE ROLE ApiRole;
GO

EXEC sp_addrolemember ApiRole, Api;
GO

CREATE SCHEMA bookings;
GO

CREATE SCHEMA users;
GO

CREATE TABLE users.UserData(
UserID INT NOT NULL IDENTITY(1,1),
FirstName NVARCHAR(255) NOT NULL,
Surname NVARCHAR(255) NOT NULL,
EmailAddress VARCHAR(254) NOT NULL,
PasswordHash VARCHAR(255) NULL,
CONSTRAINT PK_users_userData_UserID PRIMARY KEY (UserID),
CONSTRAINT UQ_users_userData_EmailAddress UNIQUE (EmailAddress)
)
GO

CREATE TABLE bookings.Venue(
VenueID INT NOT NULL IDENTITY(1,1),
VenueName VARCHAR(256) NOT NULL,
CONSTRAINT PK_bookings_Venue_VenueID PRIMARY KEY (VenueID)
);
GO

INSERT INTO bookings.Venue
(
    VenueName
)
VALUES
('Regular Venue' );
GO

CREATE TABLE bookings.Meeting(
MeetingID INT NOT NULL IDENTITY(1,1),
MeetingSubject VARCHAR(100) NOT NULL,
MeetingDetail VARCHAR(2048) NOT NULL,
StartTime DATETIME NOT NULL,
EndTime DATETIME NOT NULL,
VenueID INT NOT NULL,
Price INT NOT NULL,
CONSTRAINT PK_bookings_Meeting_MeetingID PRIMARY KEY (MeetingID),
CONSTRAINT FK_bookings_Meeting_VenueID_bookings_Venue_VenueID FOREIGN KEY (VenueID) REFERENCES bookings.Venue (VenueID)
);
GO

DECLARE @StartDate DATETIME = '2018-07-01 19:00';

INSERT INTO bookings.Meeting
(
	MeetingSubject,
	MeetingDetail,
	VenueID,
    StartTime,
    EndTime,
	Price
)
VALUES
	(  'Meeting 1', 'Lots and lots of lovely details.', 1,	@StartDate,  DATEADD(HOUR, 2, @StartDate), 0    )
,   (  'Meeting 2', 'Lots and lots of lovely details.', 1,	DATEADD(MONTH, 1, @StartDate),  DATEADD(HOUR, 2, DATEADD(MONTH, 1, @StartDate)), 0   )
,   (  'Meeting 3', 'Lots and lots of lovely details.', 1,	DATEADD(MONTH, 2, @StartDate),  DATEADD(HOUR, 2, DATEADD(MONTH, 2, @StartDate)), 0   )
,   (  'Meeting 4', 'Lots and lots of lovely details.', 1,	DATEADD(MONTH, 3, @StartDate),  DATEADD(HOUR, 2, DATEADD(MONTH, 3, @StartDate)), 0   )
,   (  'Meeting 5', 'Lots and lots of lovely details.', 1,	DATEADD(MONTH, 4, @StartDate),  DATEADD(HOUR, 2, DATEADD(MONTH, 4, @StartDate)), 0   )
,   (  'Meeting 6', 'Lots and lots of lovely details.', 1,	DATEADD(MONTH, 5, @StartDate),  DATEADD(HOUR, 2, DATEADD(MONTH, 5, @StartDate)), 0   )
GO

CREATE TABLE bookings.MethodOfPayment(
MethodOfPaymentID SMALLINT NOT NULL,
MethodOfPayment VARCHAR(100) NOT NULL,
CONSTRAINT PK_bookings_MethodOfPayment_MethodOfPaymentID PRIMARY KEY (MethodOfPaymentID)
);
GO

INSERT INTO bookings.MethodOfPayment
(
    MethodOfPaymentID,
    MethodOfPayment
)
VALUES
	(   0, 'None'	)
,	(	1, 'Card'	)
,	(	2, 'Paypal'	);
GO

CREATE TABLE bookings.BookingStatus(
BookingStatusID SMALLINT NOT NULL,
BookingStatus VARCHAR(100) NOT NULL,
CONSTRAINT PK_bookings_BookingStatus_BookingStatusID PRIMARY KEY (BookingStatusId)
);
GO

INSERT INTO bookings.BookingStatus
(
    BookingStatusID,
    BookingStatus
)
VALUES
	(   1, 'Provisional'	)
,	(	2, 'Confirmed'		)
,	(	3, 'Cancelled'		)
,	(	4, 'Timed Out'		);
GO

CREATE TABLE bookings.Booking(
BookingID INT NOT NULL IDENTITY(1,1),
UserID INT NULL,
BookingStatusID SMALLINT NOT NULL,
BookingTime DATETIME NOT NULL,
ConfirmationTime DATETIME NULL,
PaymentAmount INT NULL,
MethodOfPaymentID SMALLINT NULL,
PaymentReference VARCHAR(255) NULL,
CONSTRAINT PK_bookings_Booking_BookingID PRIMARY KEY (BookingID),
CONSTRAINT FK_bookings_Booking_UserID_users_UserData_UserID FOREIGN KEY (UserID) REFERENCES users.UserData (UserID),
CONSTRAINT FK_bookings_Booking_MethodOfPaymentID_bookings_MethodOfPayment_MethodOfPaymentID FOREIGN KEY (MethodOfPaymentID) REFERENCES bookings.MethodOfPayment (MethodOfPaymentID),
CONSTRAINT FK_bookings_Booking_BookingStatusID_bookings_BookingStatus_BookingStatusID FOREIGN KEY (BookingStatusID) REFERENCES bookings.BookingStatus (BookingStatusID)
);
GO

ALTER TABLE bookings.Booking ADD CONSTRAINT DF_bookings_Booking_Confirmed DEFAULT 1 FOR BookingStatusID;
GO

CREATE TABLE bookings.SeatBooking(
SeatBookingID INT NOT NULL IDENTITY(1,1),
BookingID INT NOT NULL,
UserID INT NULL,
AttendeeFirstName VARCHAR(255) NULL,
AttendeeSurname VARCHAR(255) NULL,
MeetingID INT NOT NULL,
RowID INT NOT NULL,
SeatID INT NOT NULL
CONSTRAINT PK_bookings_SeatBooking_SeatBookingID PRIMARY KEY (SeatBookingID),
CONSTRAINT FK_bookings_SeatBooking_UserID_users_Users_UserID FOREIGN KEY (UserID) REFERENCES users.UserData (UserID),
CONSTRAINT FK_bookings_SeatBooking_BookingID_bookings_Bookings_BookingID FOREIGN KEY (BookingID) REFERENCES bookings.Booking (BookingID),
CONSTRAINT FK_bookings_SeatBooking_MeetingID_bookings_Meeting_MeetingID FOREIGN KEY (MeetingID) REFERENCES bookings.Meeting (MeetingID),
CONSTRAINT UQ_bookings_seatBooking_MeetingID_RowID_SeatID_UserID UNIQUE (MeetingID, RowID, SeatID, UserID)
);
GO

CREATE TABLE bookings.RemovedSeatBooking(
RemovedSeatBookingID INT NOT NULL IDENTITY(1,1),
BookingID INT NOT NULL,
UserID INT NULL,
AttendeeFirstName VARCHAR(255) NULL,
AttendeeSurname VARCHAR(255) NULL,
MeetingID INT NOT NULL,
RowID INT NOT NULL,
SeatID INT NOT NULL
CONSTRAINT PK_bookings_RemovedSeatBooking_RemovedSeatBookingID PRIMARY KEY (RemovedSeatBookingID),
CONSTRAINT FK_bookings_RemovedSeatBooking_UserID_users_Users_UserID FOREIGN KEY (UserID) REFERENCES users.UserData (UserID),
CONSTRAINT FK_bookings_RemovedSeatBooking_BookingID_bookings_Bookings_BookingID FOREIGN KEY (BookingID) REFERENCES bookings.Booking (BookingID),
CONSTRAINT FK_bookings_RemovedSeatBooking_MeetingID_bookings_Meeting_MeetingID FOREIGN KEY (MeetingID) REFERENCES bookings.Meeting (MeetingID)
);
GO

CREATE TABLE bookings.AvailableRow(
RowID INT NOT NULL IDENTITY(1,1),
RowLabel VARCHAR(10) NOT NULL,
Available BIT NOT NULL,
VenueID INT NOT NULL,
CONSTRAINT PK_bookings_AvailableRow_RowID PRIMARY KEY (RowID),
CONSTRAINT FK_bookings_AvailableRow_VenueID_bookings_Venue_VenueID FOREIGN KEY (VenueID) REFERENCES bookings.Venue (VenueID)
);
GO

INSERT INTO bookings.AvailableRow
(
    RowLabel, Available, VenueID
)
VALUES
('a',1,1),
('b',1,1),
('c',1,1),
('d',1,1),
('e',1,1),
('f',1,1),
('g',1,1),
('h',1,1),
('i',1,1),
('j',1,1),
('k',0,1);
GO

CREATE TABLE bookings.AvailableSeat(
SeatID INT NOT NULL IDENTITY(1,1),
SeatLabel VARCHAR(10) NOT NULL,
Available BIT NOT NULL,
VenueID INT NOT NULL,
CONSTRAINT PK_bookings_AvailableSeat_SeatID PRIMARY KEY (SeatID),
CONSTRAINT FK_bookings_AvailableSeat_VenueID_bookings_Venue_VenueID FOREIGN KEY (VenueID) REFERENCES bookings.Venue (VenueID)
);
GO

INSERT INTO bookings.AvailableSeat
(
    SeatLabel, Available, VenueID
)
VALUES
('1',1,1),
('2',1,1),
('3',1,1),
('4',1,1),
('5',1,1),
('6',1,1),
('7',1,1),
('8',1,1),
('9',1,1),
('10',1,1);
GO

CREATE TABLE bookings.RemovedSeat(
RemovedSeatID INT NOT NULL IDENTITY(1,1),
RowID INT NOT NULL,
SeatID INT NOT NULL,
VenueID INT NOT NULL,
CONSTRAINT PK_bookings_RemovedSeat_RemovedSeatID PRIMARY KEY (RemovedSeatID),
CONSTRAINT FK_bookings_RemovedSeat_VenueID_bookings_Venue_VenueID FOREIGN KEY (VenueID) REFERENCES bookings.Venue (VenueID)
);
GO

--INSERT INTO bookings.RemovedSeat
--(
--    RowID, SeatID, VenueID
--)
--VALUES
--(   2, 2, 1 );
--GO 

CREATE VIEW bookings.AvailableSeats
AS
	SELECT venues.VenueID,
	       venues.VenueName,
		   [rows].RowID,
           [rows].RowLabel,
           seats.SeatID,
           seats.SeatLabel
	FROM bookings.AvailableRow [rows]
	CROSS APPLY bookings.AvailableSeat seats
	INNER JOIN bookings.Venue venues ON venues.VenueID = rows.VenueID
	WHERE [rows].Available = 1 AND seats.Available = 1
	AND NOT EXISTS (SELECT NULL FROM bookings.RemovedSeat removed
								WHERE [rows].RowID = removed.RowID
								AND seats.SeatID = removed.SeatID
								AND venues.VenueID = removed.VenueID);
GO

CREATE VIEW bookings.BookableSeats
AS
		SELECT meetings.MeetingID,
			   venues.VenueName,
			   totalSeats.RowID,
			   totalSeats.RowLabel AS SeatRow,
			   totalSeats.SeatID,
			   totalSeats.SeatLabel AS SeatNumber,
			   CAST(CASE WHEN bookings.SeatBookingID IS NULL THEN 1 ELSE 0 END AS BIT) AS Available
		FROM bookings.Meeting meetings
		INNER JOIN bookings.Venue venues ON venues.VenueID = meetings.VenueID
		INNER JOIN bookings.AvailableSeats totalSeats ON totalSeats.VenueID = meetings.VenueID
		LEFT OUTER JOIN bookings.SeatBooking bookings ON bookings.MeetingID = meetings.MeetingID AND bookings.RowID = totalSeats.RowID AND bookings.SeatID = totalSeats.SeatID
GO

CREATE PROCEDURE users.UpsertUser(
@FirstName VARCHAR(255),
@Surname VARCHAR(255),
@EmailAddress VARCHAR(254)
) AS
BEGIN
	BEGIN TRY
		DECLARE @userID INT;

		SET @userID = (SELECT UserID FROM users.UserData WHERE EmailAddress = @EmailAddress);

		IF (@userID IS NOT NULL)
		BEGIN
			SELECT @userID AS UserID;
		END
		ELSE BEGIN
			BEGIN TRANSACTION
			
				INSERT INTO users.UserData
				(
					FirstName,
					Surname,
					EmailAddress
				)
				SELECT @FirstName,
					@Surname,
					@EmailAddress
				WHERE NOT EXISTS (SELECT NULL FROM users.UserData WHERE EmailAddress = @EmailAddress);

				SELECT SCOPE_IDENTITY() AS UserID;

			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT <> 0)
		BEGIN
			ROLLBACK TRANSACTION;
		END;

		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON users.UpsertUser TO ApiRole;
GO

CREATE PROCEDURE bookings.CreateBooking
AS
BEGIN
	BEGIN TRY
		DECLARE @Now DATETIME = GETUTCDATE();

		INSERT INTO bookings.Booking
		( BookingTime )
		VALUES
		( @Now );

		SELECT SCOPE_IDENTITY();

	END TRY	
	BEGIN CATCH
		THROW;
	END CATCH 
END;
GO

GRANT EXECUTE ON bookings.CreateBooking TO ApiRole;
GO

CREATE PROCEDURE bookings.AttachSeatBooking(
@BookingID INT,
@MeetingID INT,
@SeatRow VARCHAR(10),
@SeatNumber VARCHAR(10)
) AS 
BEGIN
	DECLARE @RowID INT = (SELECT RowID FROM bookings.AvailableRow WHERE RowLabel = @SeatRow);
	DECLARE @SeatID INT = (SELECT SeatID FROM bookings.AvailableSeat WHERE SeatLabel = @SeatNumber);

	BEGIN TRY

		INSERT INTO bookings.SeatBooking
		(
		    BookingID,
			MeetingID,
		    RowID,
		    SeatID
		)
		VALUES
		(	@BookingID,
			@MeetingID,
		    @RowID,
		    @SeatID
		    )
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.AttachSeatBooking TO ApiRole;
GO

CREATE PROCEDURE bookings.UpdateSeatBooking(
@BookingID INT,
@MeetingID INT,
@SeatRow VARCHAR(10),
@SeatNumber VARCHAR(10),
@UserID INT,
@FirstName VARCHAR(255),
@Surname VARCHAR(255)
) AS 
BEGIN
	DECLARE @RowID INT = (SELECT RowID FROM bookings.AvailableRow WHERE RowLabel = @SeatRow);
	DECLARE @SeatID INT = (SELECT SeatID FROM bookings.AvailableSeat WHERE SeatLabel = @SeatNumber);

	BEGIN TRY

		UPDATE bookings.SeatBooking
		SET	UserID = @UserId, AttendeeFirstName = @FirstName, AttendeeSurname = @Surname
		WHERE BookingID = @BookingID AND MeetingID = @MeetingID AND RowID = @RowID AND SeatID = @SeatID 
		
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.UpdateSeatBooking TO ApiRole;
GO

CREATE PROCEDURE bookings.ConfirmBooking(
@UserId INT,
@PaymentAmount INT = 0,
@MethodOfPaymentID INT = 0,
@BookingID INT,
@PaymentReference VARCHAR(255) = NULL
) AS 
BEGIN
	BEGIN TRY
		UPDATE bookings.Booking
		SET UserID = @UserId, PaymentAmount = @PaymentAmount, MethodOfPaymentID = @MethodOfPaymentID, BookingStatusID = 2,
		ConfirmationTime = GETUTCDATE(), PaymentReference = @PaymentReference
		WHERE BookingID = @BookingID;
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.ConfirmBooking TO ApiRole;
GO

CREATE PROCEDURE bookings.CancelBooking(
@BookingID INT
) AS 
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			INSERT INTO bookings.RemovedSeatBooking
			(
				BookingID,
				UserID,
				AttendeeFirstName,
				AttendeeSurname,
				MeetingID,
				RowID,
				SeatID
			)
			SELECT BookingID,
				   UserID,
				   AttendeeFirstName,
				   AttendeeSurname,
				   MeetingID,
				   RowID,
				   SeatID
			FROM bookings.SeatBooking
			WHERE BookingID = @BookingID;

			DELETE FROM bookings.SeatBooking WHERE BookingID = @BookingID;

			UPDATE bookings.Booking SET BookingStatusID = 3 WHERE BookingID = @BookingID;
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT <> 0)
		BEGIN
			ROLLBACK TRANSACTION;
		END;

		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.CancelBooking TO ApiRole;
GO

CREATE PROCEDURE bookings.GetBookings(
@BookingID INT = NULL
) AS
BEGIN
	BEGIN TRY
		SELECT booking.BookingID,
			   users.UserID,
			   booking.BookingTime,
			   booking.BookingStatusID,
			   booking.ConfirmationTime,
			   users.FirstName,
			   users.Surname,
			   users.EmailAddress
		FROM bookings.Booking booking
		INNER JOIN users.UserData users ON users.UserID = booking.UserID
		WHERE booking.BookingID = ISNULL(@BookingID, booking.BookingID);
	END TRY
	BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.GetBookings TO ApiRole;
GO

CREATE PROCEDURE bookings.ExpireTimedOutBookings
AS
BEGIN
	BEGIN TRY
		DECLARE @BookingsToExpire TABLE (BookingID INT NOT NULL);

		INSERT INTO @BookingsToExpire ( BookingID )
		SELECT booking.BookingID
		FROM bookings.Booking booking
		WHERE booking.BookingStatusID = 1 AND booking.BookingTime < DATEADD(MINUTE, -10, GETUTCDATE());

		IF EXISTS (SELECT NULL FROM @BookingsToExpire)
		BEGIN
			BEGIN TRANSACTION
				INSERT INTO bookings.RemovedSeatBooking
				(
					BookingID,
					UserID,
					AttendeeFirstName,
					AttendeeSurname,
					MeetingID,
					RowID,
					SeatID
				)
				SELECT seatBookings.BookingID,
					   seatBookings.UserID,
					   seatBookings.AttendeeFirstName,
					   seatBookings.AttendeeSurname,
					   seatBookings.MeetingID,
					   seatBookings.RowID,
					   seatBookings.SeatID
				FROM bookings.SeatBooking seatBookings
				WHERE EXISTS (SELECT NULL FROM @BookingsToExpire expiries WHERE expiries.BookingID = seatBookings.BookingID);

				DELETE FROM bookings.SeatBooking WHERE EXISTS (SELECT NULL FROM @BookingsToExpire expiries WHERE expiries.BookingID = SeatBooking.BookingID);

				UPDATE bookings.Booking SET BookingStatusID = 4 WHERE EXISTS (SELECT NULL FROM @BookingsToExpire expiries WHERE expiries.BookingID = Booking.BookingID);

			COMMIT TRANSACTION
		END
	END TRY
    BEGIN CATCH
		IF (@@TRANCOUNT <> 0)
		BEGIN
			ROLLBACK TRANSACTION;
		END;        
		THROW;
	END CATCH
END
GO

CREATE TYPE SeatValidation AS TABLE(
	MeetingID INT NOT NULL,
	SeatRow VARCHAR(10) NOT NULL,
	SeatNumber VARCHAR(10) NOT NULL
);
GO

CREATE PROCEDURE bookings.ValidateSeatBooking(
@Seats SeatValidation READONLY
) AS 
BEGIN
	BEGIN TRY
		--Maintainance Task Prefreable
		EXEC bookings.ExpireTimedOutBookings;
	
		SELECT seats.MeetingID,
               seats.SeatRow,
               seats.SeatNumber,
               bookable.Available
		FROM @Seats seats
		LEFT JOIN bookings.BookableSeats bookable ON bookable.MeetingID = seats.MeetingID AND bookable.SeatNumber = seats.SeatNumber AND bookable.SeatRow = seats.SeatRow

	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.ValidateSeatBooking TO ApiRole;
GO

CREATE PROCEDURE bookings.GetBookingInfo(
@BookingID INT = NULL
) AS
BEGIN
	BEGIN TRY
		--Maintainance Task Prefreable
		EXEC bookings.ExpireTimedOutBookings;

		SELECT booking.BookingID,
			   booking.BookingStatusID AS BookingStatus,
			   booking.BookingTime,
			   booking.ConfirmationTime,
			   booking.PaymentAmount,
			   booking.MethodOfPaymentID,
			   booking.PaymentReference,
			   users.EmailAddress,
			   users.FirstName,
			   users.Surname
		FROM bookings.Booking booking
		LEFT JOIN users.UserData users ON users.UserID = booking.UserID
		WHERE booking.BookingID = ISNULL(@BookingID, booking.BookingID);
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.GetBookingInfo TO ApiRole;
GO

CREATE PROCEDURE bookings.GetBookingSeats(
@BookingID INT = NULL
) AS
BEGIN
	BEGIN TRY
		SELECT users.EmailAddress,
			   seats.AttendeeFirstName AS FirstName,
			   seats.AttendeeSurname AS Surname,
			   seats.MeetingID,
			   allSeats.SeatRow,
			   allSeats.SeatNumber,
			   CAST(1 AS BIT) AS Booked
		FROM bookings.SeatBooking seats
		LEFT JOIN users.UserData users ON users.UserID = seats.UserID
		INNER JOIN bookings.BookableSeats allSeats ON allSeats.RowID = seats.RowID AND allSeats.SeatID = seats.SeatID AND allSeats.MeetingID = seats.MeetingID
		WHERE seats.BookingID = ISNULL(@BookingID, seats.BookingID);
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.GetBookingSeats TO ApiRole;
GO

CREATE PROCEDURE bookings.GetMeetingEmails(
@MeetingId INT = NULL
) AS
BEGIN
	BEGIN TRY
		SELECT seats.MeetingID,
			   users.EmailAddress
		FROM bookings.SeatBooking seats
		INNER JOIN users.UserData users ON users.UserID = seats.UserID
		WHERE seats.MeetingID = ISNULL(@MeetingId, seats.MeetingId);
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.GetMeetingEmails TO ApiRole;
GO

CREATE PROCEDURE bookings.GetMeetings(
@MeetingID INT = NULL
) AS
BEGIN
	BEGIN TRY
		SELECT meetings.MeetingID,
			   meetings.MeetingSubject,
			   meetings.MeetingDetail,
			   meetings.StartTime,
			   meetings.EndTime,
			   meetings.Price,
			   venues.VenueName,
			   SUM(CASE WHEN bookings.SeatBookingID IS NULL THEN 1 ELSE 0 END) AS RemainingSeats
		FROM bookings.Meeting meetings
		INNER JOIN bookings.Venue venues ON venues.VenueID = meetings.VenueID
		INNER JOIN bookings.AvailableSeats totalSeats ON totalSeats.VenueID = meetings.VenueID
		LEFT OUTER JOIN bookings.SeatBooking bookings ON bookings.MeetingID = meetings.MeetingID AND bookings.RowID = totalSeats.RowID AND bookings.SeatID = totalSeats.SeatID
		WHERE meetings.MeetingID = ISNULL(@MeetingID, meetings.MeetingID)
		GROUP BY meetings.MeetingID,
				 meetings.MeetingSubject,
				 meetings.MeetingDetail,
				 meetings.StartTime,
				 meetings.EndTime,
				 meetings.Price,
				 venues.VenueName;
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.GetMeetings TO ApiRole;
GO

CREATE PROCEDURE bookings.GetSeats(
@MeetingID INT = NULL
) AS
BEGIN
	BEGIN TRY
		--Maintainance Task Prefreable
		EXEC bookings.ExpireTimedOutBookings;
	
		SELECT seats.MeetingID,
			   seats.VenueName,
			   seats.SeatRow,
			   seats.SeatNumber,
			   seats.Available
		FROM bookings.BookableSeats seats
		WHERE seats.MeetingID = ISNULL(@MeetingID, seats.MeetingID);
	END TRY
    BEGIN CATCH
		THROW;
	END CATCH
END
GO

GRANT EXECUTE ON bookings.GetSeats TO ApiRole;
GO