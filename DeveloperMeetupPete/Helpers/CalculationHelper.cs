﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeveloperMeetupPete.Helpers.Interfaces;
using DeveloperMeetupPete.Models.Meetings;
using Microsoft.Extensions.Logging;

namespace DeveloperMeetupPete.Helpers
{
    public class CalculationHelper : ICalculationHelper
    {
        private readonly ILogger _logger;

        public CalculationHelper(ILogger logger)
        {
            _logger = logger;
        }

        public int CalculateCost(IList<int> meetings, IList<MeetingInfo> meetingInfos)
        {
            var cost = 0;

            foreach (var meeting in meetings)
            {
                var meetingInfo = meetingInfos.FirstOrDefault(x => x.MeetingId == meeting);

                if (meetingInfo == null)
                {
                    _logger.LogError($"Data not found for meeting {meeting}: calcluation abandoned");

                    throw new Exception("Error reading cost for meeting");
                }

                cost = cost + meetingInfo.Price;
            }

            return cost;
        }
    }
}