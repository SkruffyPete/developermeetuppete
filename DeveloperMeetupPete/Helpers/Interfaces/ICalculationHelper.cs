﻿using System.Collections.Generic;
using DeveloperMeetupPete.Models.Meetings;

namespace DeveloperMeetupPete.Helpers.Interfaces
{
    public interface ICalculationHelper
    {
        int CalculateCost(IList<int> meetings, IList<MeetingInfo> meetingInfos);
    }
}