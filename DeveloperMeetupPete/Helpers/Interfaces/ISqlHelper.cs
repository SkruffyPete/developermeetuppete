﻿using System.Collections.Generic;
using System.Data;
using DeveloperMeetupPete.Models.Bookings;

namespace DeveloperMeetupPete.Helpers.Interfaces
{
    public interface ISqlHelper
    {
        DataTable CreateSeatDataTable(IList<ProvisionalSeatBooking> seatBooking);
    }
}