﻿using System.Collections.Generic;
using System.Data;
using DeveloperMeetupPete.Helpers.Interfaces;
using DeveloperMeetupPete.Models.Bookings;

namespace DeveloperMeetupPete.Helpers
{
    public class SqlHelper : ISqlHelper
    {
        public DataTable CreateSeatDataTable(IList<ProvisionalSeatBooking> seatBooking)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("MeetingID", typeof(int));
            dataTable.Columns.Add("SeatRow", typeof(string));
            dataTable.Columns.Add("SeatNumber", typeof(string));

            foreach (var seat in seatBooking)
            {
                var row = dataTable.NewRow();
                row["MeetingID"] = seat.MeetingId;
                row["SeatRow"] = seat.SeatRow;
                row["SeatNumber"] = seat.SeatNumber;
                dataTable.Rows.Add(row);
            }

            return dataTable;
        }
    }
}