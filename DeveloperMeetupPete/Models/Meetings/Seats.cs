﻿namespace DeveloperMeetupPete.Models.Meetings
{
    public class Seats
    {
        public int MeetingId { get; set; }

        public string VenueName { get; set; }

        public string SeatRow { get; set; }

        public string SeatNumber { get; set; }

        public bool Available { get; set; }
    }
}