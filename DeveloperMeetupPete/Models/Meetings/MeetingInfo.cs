﻿using System;

namespace DeveloperMeetupPete.Models.Meetings
{
    public class MeetingInfo
    {
        public int MeetingId { get; set; }

        public string MeetingSubject { get; set; }

        public string MeetingDetail { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public int Price { get; set; }

        public string VenueName { get; set; }

        public int RemainingSeats { get; set; }
    }
}