﻿using System;
using System.Collections.Generic;

namespace DeveloperMeetupPete.Models.Meetings
{
    public class Meeting
    {
        public MeetingInfo MeetingInfo { get; set; }

        public IEnumerable<Seats> Seats { get; set; }
    }
}