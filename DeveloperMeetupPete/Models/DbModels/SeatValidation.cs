﻿namespace DeveloperMeetupPete.Models.DbModels
{
    public class SeatValidationResponse
    {
        public int MeetingId { get; set; }

        public string SeatRow { get; set; }

        public string SeatNumber { get; set; }

        public bool Available { get; set; }
    }
}