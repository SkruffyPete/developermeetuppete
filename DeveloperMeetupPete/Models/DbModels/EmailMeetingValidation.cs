﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeveloperMeetupPete.Models.DbModels
{
    public class EmailMeetingValidation
    {
        public int MeetingId { get; set; }

        public string EmailAddress { get; set; }
    }
}