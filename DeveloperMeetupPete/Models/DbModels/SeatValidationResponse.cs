﻿namespace DeveloperMeetupPete.Models.DbModels
{
    public class SeatValidation
    {
        public int MeetingId { get; set; }

        public string SeatRow { get; set; }

        public string SeatNumber { get; set; }
    }
}