﻿using System.Collections.Generic;
using DeveloperMeetupPete.Constants;

namespace DeveloperMeetupPete.Models.Bookings
{
    public class Booking
    {
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string EmailAddress { get; set; }

        public int PaymentAmount { get; set; }

        public MethodOfPayment? MethodOfPayment { get; set; }

        public string PaymentReference { get; set; }

        public List<SeatBooking> SeatBookings { get; set; }

        public BookingStatus BookingStatus { get; set; }
    }
}