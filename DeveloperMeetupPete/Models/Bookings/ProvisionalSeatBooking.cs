﻿using System;

namespace DeveloperMeetupPete.Models.Bookings
{
    public class ProvisionalSeatBooking : IEquatable<ProvisionalSeatBooking>
    {
        public int MeetingId { get; set; }

        public string SeatRow { get; set; }

        public string SeatNumber { get; set; }

        public bool Equals(ProvisionalSeatBooking booking)
        {
            if (booking == null)
            {
                return false;
            }

            return MeetingId == booking.MeetingId
                   && string.Equals(SeatRow, booking.SeatRow, StringComparison.InvariantCultureIgnoreCase)
                   && string.Equals(SeatNumber, booking.SeatNumber, StringComparison.InvariantCultureIgnoreCase);

        }
    }
}