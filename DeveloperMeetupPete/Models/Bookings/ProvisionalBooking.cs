﻿using System.Collections.Generic;

namespace DeveloperMeetupPete.Models.Bookings
{
    public class ProvisionalBooking
    {
        public List<ProvisionalSeatBooking> SeatBookings { get; set; }
    }
}