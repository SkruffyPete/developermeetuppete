﻿namespace DeveloperMeetupPete.Models.Bookings
{
    public class SeatBooking : ProvisionalSeatBooking
    {
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string EmailAddress { get; set; }

        public bool Booked { get; set; }
    }
}