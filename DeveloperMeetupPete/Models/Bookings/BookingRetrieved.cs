﻿using System;

namespace DeveloperMeetupPete.Models.Bookings
{
    public class BookingRetrieved : Booking
    {
        public int BookingId { get; set; }

        public DateTime BookingTime { get; set; }

        public DateTime ConfirmationTime { get; set; }
    }
}