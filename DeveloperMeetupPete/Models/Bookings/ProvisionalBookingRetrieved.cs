﻿using System;

namespace DeveloperMeetupPete.Models.Bookings
{
    public class ProvisionalBookingRetrieved : ProvisionalBooking
    {
        public int BookingId { get; set; }

        public DateTime BookingTime { get; set; }
    }
}