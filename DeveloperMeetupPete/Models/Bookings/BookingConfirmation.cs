﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeveloperMeetupPete.Models.Bookings
{
    public class BookingConfirmation : Booking
    {
        public int BookingId { get; set; }

        public PaymentObject PaymentObject { get; set; }
    }
}