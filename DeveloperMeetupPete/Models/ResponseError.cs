﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeveloperMeetupPete.Models
{
    public class ResponseError
    {
        public string Error { get; set; }

        public string Value { get; set; }
    }
}