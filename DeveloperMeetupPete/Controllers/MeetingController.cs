﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using DeveloperMeetupPete.Models;
using DeveloperMeetupPete.Models.Meetings;
using DeveloperMeetupPete.Providers.Interfaces;
using Microsoft.Extensions.Logging;
using Swagger.Net.Annotations;

namespace DeveloperMeetupPete.Controllers
{
    /// <summary>
    /// All data relating to meetings.
    /// </summary>
    [RoutePrefix("api/meetings")]
    public class MeetingController : ApiController
    {
        private readonly IMeetingProvider _meetingProvider;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="meetingProvider">The meeting provider</param>
        /// <param name="logger">The logger</param>
        public MeetingController(IMeetingProvider meetingProvider, ILogger logger)
        {
            _meetingProvider = meetingProvider;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves all meetings
        /// </summary>
        /// <returns>All meetings</returns>
        [Route, HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<Meeting>))]
        public async Task<IHttpActionResult> GetMeetings()
        {
            var meetingInfos = await _meetingProvider.RetrieveAllMeetings().ConfigureAwait(false);

            var meetingInfoList = meetingInfos.ToList();

            if (!meetingInfoList.Any())
            {
                return NotFound();
            }

            var seats = await _meetingProvider.RetrieveAllSeats().ConfigureAwait(false);

            var meetings = meetingInfoList.Select(meeting => new Meeting()
            {
                MeetingInfo = meeting,
                Seats = seats.Where(x => x.MeetingId == meeting.MeetingId)
            });

            return Ok(meetings);
        }

        /// <summary>
        /// Retrieves the metadata for all meetings
        /// </summary>
        /// <returns>All meetings</returns>
        [Route("summary"), HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<MeetingInfo>))]
        public async Task<IHttpActionResult> GetMeetingSummaries()
        {
            var meeting = await _meetingProvider.RetrieveAllMeetings().ConfigureAwait(false);

            if (!meeting.Any())
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Returns the data for a single meeting
        /// </summary>
        /// <param name="meetingId">The meeting ID</param>
        /// <returns>The meeting</returns>
        [Route("{meetingId}"), HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Meeting))]
        public async Task<IHttpActionResult> GetMeeting(int meetingId)
        {
            var meetingInfo = await _meetingProvider.RetrieveMeeting(meetingId).ConfigureAwait(false);

            if (meetingInfo == null)
            {
                return NotFound();
            }

            var seats = await _meetingProvider.RetrieveSeats(meetingId).ConfigureAwait(false);

            var meeting = new Meeting()
            {
                MeetingInfo = meetingInfo,
                Seats = seats
            };

            return Ok(meeting);
        }

        /// <summary>
        /// Returns the metadata for a single meeting
        /// </summary>
        /// <param name="meetingId">The meeting ID</param>
        /// <returns>The meeting</returns>
        [Route("{meetingId}/summary"), HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(MeetingInfo))]
        public async Task<IHttpActionResult> GetMeetingSummary(int meetingId)
        {
            var meetingInfo = await _meetingProvider.RetrieveMeeting(meetingId).ConfigureAwait(false);

            if (meetingInfo == null)
            {
                return NotFound();
            }

            return Ok(meetingInfo);
        }

        /// <summary>
        /// Retrieves the seat data for a single meeting
        /// </summary>
        /// <param name="meetingId">The meeting ID</param>
        /// <returns>The meeting seats</returns>
        [Route("{meetingId}/seats"), HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<Seats>))]
        public async Task<IHttpActionResult> GetMeetingWithSeats(int meetingId)
        {
            var seats = await _meetingProvider.RetrieveSeats(meetingId).ConfigureAwait(false);
            var seatsList = seats.ToList();

            if (!seatsList.Any())
            {
                return NotFound();
            }

            return Ok(seatsList);
        }
    }
}