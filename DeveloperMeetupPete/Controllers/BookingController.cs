﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DeveloperMeetupPete.BaseClasses;
using DeveloperMeetupPete.Constants;
using DeveloperMeetupPete.Helpers.Interfaces;
using DeveloperMeetupPete.Models;
using DeveloperMeetupPete.Models.Bookings;
using DeveloperMeetupPete.Models.Meetings;
using DeveloperMeetupPete.Providers.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Swagger.Net.Annotations;

namespace DeveloperMeetupPete.Controllers
{
    /// <summary>
    /// Allows bookings to be made
    /// </summary>
    [RoutePrefix("api/bookings")]
    public class BookingController : CustomBaseApiController
    {
        #region Constructor

        private readonly IBookingProvider _bookingProvider;
        private readonly IMeetingProvider _meetingProvider;
        private readonly IPaymentProvider _paymentProvider;
        private readonly ICalculationHelper _calculationHelper;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bookingProvider">Booking provider</param>
        /// <param name="meetingProvider">Meeting Provider</param>
        /// <param name="paymentProvider">Payment Provider</param>
        /// <param name="calculationHelper">THe calculation helper</param>
        /// <param name="logger">Logger</param>
        public BookingController(IBookingProvider bookingProvider, IMeetingProvider meetingProvider, IPaymentProvider paymentProvider, ICalculationHelper calculationHelper, ILogger logger)
        {
            _bookingProvider = bookingProvider;
            _meetingProvider = meetingProvider;
            _paymentProvider = paymentProvider;
            _calculationHelper = calculationHelper;
            _logger = logger;
        }

        #endregion

        #region routes

        /// <summary>
        /// Creates a new provisional booking
        /// </summary>
        /// <param name="booking">The booking object</param>
        /// <returns>The completed booking ID</returns>
        [Route, HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BookingRetrieved))]
        [SwaggerResponse(HttpStatusCode.BadRequest)]
        public async Task<IHttpActionResult> CreateBooking(ProvisionalBooking booking)
        {
            _logger.LogDebug($"Create booking with for {booking.SeatBookings.Count} seats.");

            var meetings = new List<int>();
            booking.SeatBookings.ForEach(x => meetings.Add(x.MeetingId));
            var distinctMeetings = meetings.Distinct().ToList();

            if (booking.SeatBookings == null || !booking.SeatBookings.Any())
            {
                _logger.LogDebug("Create booking contains no seats.");
                return BadRequest(CommonConstants.NoSeatsResponse);
            }

            if (booking.SeatBookings.Count != booking.SeatBookings.GroupBy(x => new {x.SeatNumber, x.SeatRow, x.MeetingId}).Count())
            {
                _logger.LogDebug("Create booking contains duplicate seats.");
                return BadRequest(CommonConstants.DuplicateSeatsResponse);
            }

            if (booking.SeatBookings.Count > CommonConstants.MaximumSeatsPerBooking)
            {
                _logger.LogDebug("Create booking contains too many seats.");
                return BadRequest(CommonConstants.MaximumSeatsResponse);
            }

            var meetingInfos = await GetMeetingInfosForBooking(distinctMeetings).ConfigureAwait(false);

            if (meetingInfos.Count != distinctMeetings.Count)
            {
                _logger.LogDebug("Create booking contains invalid meeting ids.");
                return BadRequest(CommonConstants.MeetingNotFoundResponse);
            }
            
            var seatValidations = await _bookingProvider.ValidateSeats(booking.SeatBookings).ConfigureAwait(false);

            if (seatValidations.Any(x => !x.Available))
            {
                _logger.LogDebug("Create booking references unavailble seats.");
                return BadRequest(CommonConstants.SeatsUnavailableResponse);
            }

            var bookingId = await _bookingProvider.SaveProvisionalBooking(booking).ConfigureAwait(false);

            _logger.LogDebug("Create booking complete for seats.");
            
            return await GetBooking(bookingId).ConfigureAwait(false);
        }

        /// <summary>
        /// Confirms a booking
        /// </summary>
        /// <param name="bookingId">The booking ID to be confirmed</param>
        /// <param name="paymentObject">The payment object (to charge)</param>
        /// <returns>OK</returns>
        [Route("{bookingId}"), HttpPut]
        [SwaggerResponse(HttpStatusCode.OK)]
        public async Task<IHttpActionResult> ConfirmBooking(int bookingId, BookingConfirmation booking)
        {
            if (bookingId != booking.BookingId)
            {
                return BadRequest(CommonConstants.BookingIdMismatchResponse);
            }

            var meetings = new List<int>();
            booking.SeatBookings.ForEach(x => meetings.Add(x.MeetingId));
            var distinctMeetings = meetings.Distinct().ToList();

            var provisionalBooking = await _bookingProvider.RetrieveBooking<Booking>(booking.BookingId).ConfigureAwait(false);

            if (provisionalBooking.BookingStatus != BookingStatus.Provisional)
            {
                return BadRequest(CommonConstants.BadBookingStatusIdResponse);
            }

            if (!await ValidateBookingSeats(booking.BookingId, booking.SeatBookings).ConfigureAwait(false))
            {
                return BadRequest(CommonConstants.SeatsMismatchResponse);
            }

            if (booking.SeatBookings.Count != booking.SeatBookings.GroupBy(x => new { x.EmailAddress, x.MeetingId }).Count())
            {
                return BadRequest(CommonConstants.DuplicateEmailAddresses);
            }

            var duplicateEmails = await EmailsAlreadyRegistered(booking.SeatBookings).ConfigureAwait(false);
            var duplicateEmailList = duplicateEmails.ToList();

            if (duplicateEmailList.Any())
            {
                return ResponseMessage(GetErrorResponse(duplicateEmailList));
            }

            var meetingInfos = await GetMeetingInfosForBooking(distinctMeetings).ConfigureAwait(false);

            var cost = _calculationHelper.CalculateCost(meetings, meetingInfos);

            if (cost != booking.PaymentAmount)
            {
                return BadRequest(CommonConstants.IncorrectPaymentResponse);
            }

            if (cost != 0)
            {
                if (booking.PaymentObject == null)
                {
                    return BadRequest(CommonConstants.NoPaymentResponse);
                }

                booking.PaymentReference = await _paymentProvider.ProcessPayment(booking.PaymentObject).ConfigureAwait(false);
            }

            await _bookingProvider.ConfirmBooking(booking).ConfigureAwait(false);

            return NoContent(CommonConstants.BookingConfirmedResponse);
        }

        /// <summary>
        /// Creates the error response message including error object
        /// </summary>
        /// <param name="duplicateEmailList">List of duplicated emails</param>
        /// <returns>HttpeResponseMessage</returns>
        private static HttpResponseMessage GetErrorResponse(IEnumerable<SeatBooking> duplicateEmailList)
        {
            var error = duplicateEmailList.ToList().Select(duplicateEmail => new ResponseError
                {
                    Error = $"Email already registered to meeting:{duplicateEmail.MeetingId}",
                    Value = duplicateEmail.EmailAddress
                })
                .ToList();

            return new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(JsonConvert.SerializeObject(error), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// Retrieves the information for a booking
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        [Route("{bookingId}"), HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BookingRetrieved))]
        [SwaggerResponse(HttpStatusCode.BadRequest)]
        public async Task<IHttpActionResult> GetBooking(int bookingId)
        {
            var booking = await _bookingProvider.RetrieveBooking<BookingRetrieved>(bookingId).ConfigureAwait(false);
            
            if (booking == null)
            {
                return NotFound();
            }

            var seatBookings = await _bookingProvider.RetrieveSeatBookings<SeatBooking>(bookingId).ConfigureAwait(false);

            booking.SeatBookings = seatBookings.ToList();
            
            return Ok(booking);
        }

        /// <summary>
        /// Deletes a booking
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        [Route("{bookingId}"), HttpDelete]
        [SwaggerResponse(HttpStatusCode.NoContent)]
        public async Task<IHttpActionResult> RemoveBooking(int bookingId)
        {
            await _bookingProvider.RemoveBooking(bookingId).ConfigureAwait(false);

            return NoContent("Booking deleted");
        }

        #endregion

        #region Private Methods

        private async Task<IEnumerable<SeatBooking>> EmailsAlreadyRegistered(IEnumerable<SeatBooking> booking)
        {
            var meetingEmails = await _meetingProvider.RetrieveAllMeetingEmails().ConfigureAwait(false);

            return booking.Where(x => meetingEmails.Any(y => y.MeetingId == x.MeetingId && y.EmailAddress == x.EmailAddress));
        }

        /// <summary>
        /// Validates the seats to update upon confirmation vs the seats stored by the provisional booking
        /// </summary>
        /// <param name="bookingId">The provisional booking Id</param>
        /// <param name="seats">The seats from the booking confirmation</param>
        /// <returns>Whether the seats match or not</returns>
        private async Task<bool> ValidateBookingSeats(int bookingId, IEnumerable<SeatBooking> seats)
        {
            var provisionalSeatBookings = await _bookingProvider.RetrieveSeatBookings<ProvisionalSeatBooking>(bookingId).ConfigureAwait(false);

            var seatBookings = seats.Select(seatBooking => new ProvisionalSeatBooking()
                {
                    MeetingId = seatBooking.MeetingId,
                    SeatRow = seatBooking.SeatRow,
                    SeatNumber = seatBooking.SeatNumber
                })
                .ToList();

            var provisionalSeatBookingsList = provisionalSeatBookings.ToList();

            return provisionalSeatBookingsList.Any() 
                   && provisionalSeatBookingsList.Count == seatBookings.Count
                   && provisionalSeatBookingsList.All(provisionalSeatBooking => seatBookings.Any(x => x.Equals(provisionalSeatBooking)));
        }

        /// <summary>
        /// Gets all the meeting information for all meetings in a booking
        /// </summary>
        /// <param name="distinctMeetings">The list of meetings to retireve information for</param>
        /// <returns>List of meetingInfos</returns>
        private async Task<List<MeetingInfo>> GetMeetingInfosForBooking(IEnumerable<int> distinctMeetings)
        {
            var meetingInfos = new SynchronizedCollection<MeetingInfo>();

            var tasks = distinctMeetings.ToList().Select(meeting => GetMeetingInfo(meetingInfos, meeting));
            await Task.WhenAll(tasks);

            return meetingInfos.ToList();
        }

        /// <summary>
        /// Adds meetingInfo to the list if the meeting exists
        /// </summary>
        /// <param name="meetingInfos">The list of MeetingInfos</param>
        /// <param name="meetingId">The meetingId to add to the list</param>
        /// <returns>Task</returns>
        private async Task GetMeetingInfo(ICollection<MeetingInfo> meetingInfos, int meetingId)
        {
            var meetingInfo = await _meetingProvider.RetrieveMeeting(meetingId).ConfigureAwait(false);

            if (meetingInfo != null)
            {
                meetingInfos.Add(meetingInfo);
            }
        }

        #endregion
    }
}