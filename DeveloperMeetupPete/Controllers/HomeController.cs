﻿using System.Net;
using System.Web.Http;
using System.Reflection;
using Swagger.Net.Annotations;

namespace DeveloperMeetupPete.Controllers
{
    /// <summary>
    /// Home
    /// </summary>
    [RoutePrefix("api")]
    public class HomeController : ApiController
    {
        /// <summary>
        /// Checks that the Booking API is alive
        /// </summary>
        /// <returns>Keepalive Message</returns>
        [Route("keepAlive"), HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(string))]
        public IHttpActionResult KeepAlive()
        {
            return Ok($"The developer meetup API is Alive. {Assembly.GetExecutingAssembly().GetName().Version}");
        }
    }
}