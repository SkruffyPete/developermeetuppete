﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeveloperMeetupPete.Models;
using DeveloperMeetupPete.Models.Bookings;
using DeveloperMeetupPete.Models.DbModels;

namespace DeveloperMeetupPete.Providers.Interfaces
{
    public interface IBookingProvider
    {
        Task<int> SaveProvisionalBooking(ProvisionalBooking bookingRequest);

        Task ConfirmBooking(BookingConfirmation booking);
        
        Task<T> RetrieveBooking<T>(int bookingId);

        Task<IEnumerable<T>> RetrieveSeatBookings<T>(int bookingId);

        Task<IEnumerable<SeatValidationResponse>> ValidateSeats(List<ProvisionalSeatBooking> seatBooking);

        Task RemoveBooking(int bookingId);
    }
}