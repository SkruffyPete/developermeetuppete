﻿using System.Threading.Tasks;
using DeveloperMeetupPete.Models.Bookings;

namespace DeveloperMeetupPete.Providers.Interfaces
{
    public interface IPaymentProvider
    {
        Task<string> ProcessPayment(PaymentObject payment);
    }
}