﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeveloperMeetupPete.Models;
using DeveloperMeetupPete.Models.DbModels;
using DeveloperMeetupPete.Models.Meetings;

namespace DeveloperMeetupPete.Providers.Interfaces
{
    public interface IMeetingProvider
    {
        Task<MeetingInfo> RetrieveMeeting(int meetingId);

        Task<IEnumerable<MeetingInfo>> RetrieveAllMeetings();

        Task<IEnumerable<Seats>> RetrieveSeats(int meetingId);

        Task<IEnumerable<Seats>> RetrieveAllSeats();

        Task<IEnumerable<EmailMeetingValidation>> RetrieveAllMeetingEmails();
    }
}