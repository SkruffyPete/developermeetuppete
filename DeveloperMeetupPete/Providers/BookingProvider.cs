﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Dapper;
using Dapper.Abstractions;
using DeveloperMeetupPete.Constants;
using DeveloperMeetupPete.Helpers;
using DeveloperMeetupPete.Helpers.Interfaces;
using DeveloperMeetupPete.Models;
using DeveloperMeetupPete.Models.Bookings;
using DeveloperMeetupPete.Models.DbModels;
using DeveloperMeetupPete.Providers.Interfaces;
using Microsoft.Extensions.Logging;

namespace DeveloperMeetupPete.Providers
{
    public class BookingProvider : IBookingProvider
    {
        private readonly IDbExecutorFactory _database;
        private readonly ILogger _logger;
        private readonly ISqlHelper _sqlHelper;

        public BookingProvider(IDbExecutorFactory dbExecutorFactory, ISqlHelper sqlHelper, ILogger logger)
        {
            _database = dbExecutorFactory;
            _sqlHelper = sqlHelper;
            _logger = logger;
        }

        public async Task<int> SaveProvisionalBooking(ProvisionalBooking booking)
        {
            var bookingId = 0;

            try
            {
                using (var dbExecutor = _database.CreateExecutor())
                {
                    bookingId = await dbExecutor.QuerySingleAsync<int>(CommonConstants.ProcCreateBooking, commandType: CommandType.StoredProcedure);
                }

                var tasks = booking.SeatBookings.ToList().Select(x => SaveSeatsToBooking(bookingId, x));
                await Task.WhenAll(tasks);
            }
            catch(Exception exception)
            {
                _logger.LogError($"Failed to create booking {bookingId}. Exception follows: {exception}");

                if (bookingId != 0)
                {
                    await RemoveBooking(bookingId).ConfigureAwait(false);
                }

                throw;
            }

            return bookingId;
        }

        public async Task ConfirmBooking(BookingConfirmation booking)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var userId = await UpsertUser(booking.FirstName, booking.Surname, booking.EmailAddress);

                using (var dbExecutor = _database.CreateExecutor())
                {
                    await dbExecutor.ExecuteAsync(CommonConstants.ProcConfirmBooking, new
                        {
                            userId,
                            booking.PaymentAmount,
                            MethodOfPaymentId = booking.MethodOfPayment,
                            booking.BookingId,
                            booking.PaymentReference
                    }, commandType: CommandType.StoredProcedure);
                }

                var tasks = booking.SeatBookings.ToList().Select(x => UpdateSeatsOnBooking(booking.BookingId, x));
                await Task.WhenAll(tasks);

                transactionScope.Complete();
            }
        }

        public async Task<IEnumerable<SeatValidationResponse>> ValidateSeats(List<ProvisionalSeatBooking> seatBooking)
        {
            var seatDataTable = _sqlHelper.CreateSeatDataTable(seatBooking);

            using (var dbExecutor = _database.CreateExecutor())
            {
                return await dbExecutor.QueryAsync<SeatValidationResponse>(CommonConstants.ProcValidateSeatBooking,
                    new
                    {
                        Seats = seatDataTable.AsTableValuedParameter(CommonConstants.TvpSeatValidation)
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        private async Task SaveSeatsToBooking(int bookingId, ProvisionalSeatBooking seatBooking)
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                await dbExecutor.ExecuteAsync(CommonConstants.ProcAttachSeatBooking,
                    new
                    {
                        bookingId,
                        seatBooking.MeetingId,
                        seatBooking.SeatRow,
                        seatBooking.SeatNumber
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        private async Task UpdateSeatsOnBooking(int bookingId, SeatBooking seatBooking)
        {
            var attendeeId = await UpsertUser(seatBooking.FirstName, seatBooking.Surname, seatBooking.EmailAddress);

            using (var dbExecutor = _database.CreateExecutor())
            {
                await dbExecutor.ExecuteAsync(CommonConstants.ProcUpdateSeatBooking,
                    new
                    {
                        bookingId,
                        seatBooking.MeetingId,
                        seatBooking.SeatRow,
                        seatBooking.SeatNumber,
                        UserId = attendeeId,
                        FirstName = seatBooking.FirstName.Trim(),
                        Surname = seatBooking.Surname.Trim()
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        private async Task<int> UpsertUser(string firstName, string surname, string emailAddress)
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                return await dbExecutor.QuerySingleAsync<int>(CommonConstants.ProcUpsertUser,
                    new
                    {
                        EmailAddress = emailAddress.Trim().ToLowerInvariant(),
                        FirstName = firstName.Trim(),
                        Surname = surname.Trim()
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<T> RetrieveBooking<T>(int bookingId)
        {
            T booking;

            using (var dbExecutor = _database.CreateExecutor())
            {
                booking = await dbExecutor.QueryFirstOrDefaultAsync<T>(CommonConstants.ProcGetBookingInfo, new
                {
                    bookingId
                }, commandType: CommandType.StoredProcedure);
            }

            return booking;
        }

        public async Task<IEnumerable<T>> RetrieveSeatBookings<T>(int bookingId)
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                return await dbExecutor.QueryAsync<T>(CommonConstants.ProcGetBookingSeats, new
                {
                    bookingId
                }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task RemoveBooking(int bookingId)
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                await dbExecutor.ExecuteAsync(CommonConstants.ProcCancelBooking, new
                {
                    bookingId
                }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}