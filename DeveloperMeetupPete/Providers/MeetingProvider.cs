﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper.Abstractions;
using DeveloperMeetupPete.Constants;
using DeveloperMeetupPete.Models.DbModels;
using DeveloperMeetupPete.Models.Meetings;
using DeveloperMeetupPete.Providers.Interfaces;
using Microsoft.Extensions.Logging;

namespace DeveloperMeetupPete.Providers
{
    public class MeetingProvider : IMeetingProvider
    {
        private readonly IDbExecutorFactory _database;
        private readonly ILogger _logger;
        
        public MeetingProvider(IDbExecutorFactory dbExecutorFactory, ILogger logger)
        {
            _database = dbExecutorFactory;

            _logger = logger;
        }

        public async Task<MeetingInfo> RetrieveMeeting(int meetingId)
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                return await dbExecutor.QueryFirstOrDefaultAsync<MeetingInfo>(CommonConstants.ProcGetMeetings, new
                {
                    meetingId
                }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<MeetingInfo>> RetrieveAllMeetings()
        {
            IEnumerable<MeetingInfo> meeting;

            using (var dbExecutor = _database.CreateExecutor())
            {
                meeting = await dbExecutor.QueryAsync<MeetingInfo>(CommonConstants.ProcGetMeetings, commandType: CommandType.StoredProcedure);
            }

            return meeting;
        }

        public async Task<IEnumerable<Seats>> RetrieveSeats(int meetingId)
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                return await dbExecutor.QueryAsync<Seats>(CommonConstants.ProcGetSeats, new
                {
                    meetingId
                }, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Seats>> RetrieveAllSeats()
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                return await dbExecutor.QueryAsync<Seats>(CommonConstants.ProcGetSeats, commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<EmailMeetingValidation>> RetrieveAllMeetingEmails()
        {
            using (var dbExecutor = _database.CreateExecutor())
            {
                return await dbExecutor.QueryAsync<EmailMeetingValidation>(CommonConstants.ProcGetMeetingEmails, commandType: CommandType.StoredProcedure);
            }
        }
    }
}