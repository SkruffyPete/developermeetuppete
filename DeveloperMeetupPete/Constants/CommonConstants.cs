﻿namespace DeveloperMeetupPete.Constants
{
    public class CommonConstants
    {
        public const int MaximumSeatsPerBooking = 4;
        public const string ConfigDbConnectionString = "DbConnectionString";

        // Database Constants
        public const string ProcGetMeetings = "bookings.GetMeetings";
        public const string ProcGetSeats = "bookings.GetSeats";
        public const string ProcCreateBooking = "bookings.CreateBooking";
        public const string ProcConfirmBooking = "bookings.ConfirmBooking";
        public const string ProcValidateSeatBooking = "bookings.ValidateSeatBooking";
        public const string ProcAttachSeatBooking = "bookings.AttachSeatBooking";
        public const string ProcUpdateSeatBooking = "bookings.UpdateSeatBooking";
        public const string ProcUpsertUser = "users.UpsertUser";
        public const string ProcGetBookingInfo = "bookings.GetBookingInfo";
        public const string ProcGetBookingSeats = "bookings.GetBookingSeats";
        public const string ProcGetMeetingEmails = "bookings.GetMeetingEmails";
        public const string ProcCancelBooking = "bookings.CancelBooking";

        public const string TvpSeatValidation = "SeatValidation";

        // Response Messages
        public const string NoSeatsResponse = "Booking request must include seat bookings.";
        public const string DuplicateSeatsResponse = "Booking contains duplicate seats.";
        public const string MeetingNotFoundResponse = "Meeting not found.";
        public const string SeatsUnavailableResponse = "Seats not available.";
        public static readonly string MaximumSeatsResponse = $"Cannot book more than {MaximumSeatsPerBooking} seats at once.";

        public const string BookingIdMismatchResponse = "BookingId mismatch.";
        public const string DuplicateEmailAddresses = "Attendee email addresses must be unique for each session.";
        public const string BadBookingStatusIdResponse = "Can only confirm a provisional booking.";
        public const string SeatsMismatchResponse = "Seats do not match provisional booking.";

        public const string IncorrectPaymentResponse = "Payment amount incorrect.";
        public const string NoPaymentResponse = "Payment not provided.";
        public const string BookingConfirmedResponse = "Booking confirmed";
    }
}