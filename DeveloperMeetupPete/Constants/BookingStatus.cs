﻿namespace DeveloperMeetupPete.Constants
{
    public enum BookingStatus
    {
        Provisional = 1,
        Confirmed = 2,
        Cancelled = 3,
        TimedOut = 4
    }
}