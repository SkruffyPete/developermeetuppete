﻿namespace DeveloperMeetupPete.Constants
{
    public enum MethodOfPayment
    {
        None = 0,
        Card = 1,
        Paypal = 2
    }
}