﻿using System;
using System.IO;
using System.Web.Http;
using DeveloperMeetupPete.Filters;
using Microsoft.Extensions.Logging;
using Unity;

namespace DeveloperMeetupPete
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Filters.Add(new ExceptionFilter(UnityConfig.Container.Resolve<ILogger>()));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DeveloperMeetupPet",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Tweak to Pathing for Libsodium
            var path = Environment.GetEnvironmentVariable("PATH");
            var binDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Bin");
            Environment.SetEnvironmentVariable("PATH", path + ";" + binDir);
        }
    }
}
