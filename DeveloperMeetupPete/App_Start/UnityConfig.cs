using System;
using System.Configuration;
using Dapper.Abstractions;
using DeveloperMeetupPete.Constants;
using DeveloperMeetupPete.Helpers;
using DeveloperMeetupPete.Helpers.Interfaces;
using DeveloperMeetupPete.Providers;
using DeveloperMeetupPete.Providers.Interfaces;
using Unity;

namespace DeveloperMeetupPete
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterInstances(container);
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<IBookingProvider, BookingProvider>();
            container.RegisterType<IMeetingProvider, MeetingProvider>();
            container.RegisterType<IPaymentProvider, PaymentProvider>();
            container.RegisterType<ISqlHelper, SqlHelper>();
            container.RegisterType<ICalculationHelper, CalculationHelper>();
        }

        public static void RegisterInstances(IUnityContainer container)
        {
            var logProvider = new Useful.LoggerFactory.Log4net.Log4NetProvider();

            //var log4NetConfig = Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory, "DeveloperMeetup.Log4net.xml", SearchOption.AllDirectories).FirstOrDefault();
            var logger = logProvider.CreateLogger("DevMeetupLogger");

            container.RegisterInstance(logger);

            var connectionString = ConfigurationManager.AppSettings[CommonConstants.ConfigDbConnectionString];
            
            var dbExecutorFactory = new SqlExecutorFactory(connectionString);

            container.RegisterInstance<IDbExecutorFactory>(dbExecutorFactory);
        }
    }
}